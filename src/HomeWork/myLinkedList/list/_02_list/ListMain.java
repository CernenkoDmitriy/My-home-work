package HomeWork.myLinkedList.list._02_list;


import HomeWork.myLinkedList.list._02_list.list.MyLinkedList;

import java.util.Iterator;

public class ListMain {

    public static void main(String[] args) {
//        testAddToTail();
//        testAddIncorrectIndex();
//        testAddIndexBounds();
//        testAddInside();
//        testDeleteFromHead();
//        testDeleteFromTail();
//        testDelete();

    }
      /*  //Do not do this
        //for (int i = 0; i < myLinkedList.size(); ++i) {
        //System.out.println(myLinkedList.get(i));
        //}

        //Do this:
        //Step 1: Get iterator
        //Iterator<Integer> iterator = myLinkedList.iterator();
        //Step 2: Run while loop
     /* while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        System.out.println("-----------");
        //Цикл for each - выполняет тоже самое что и конструкция выше
        for (Integer i: myLinkedList) {
            System.out.println(i);
        }*/

    //  }



   /* private static void testAddInside() {
        MyLinkedList myLinkedList = new MyLinkedList();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        myLinkedList.add(10, 1);
        myLinkedList.add(20, 6);
        System.out.println(myLinkedList);
        System.out.println("Done testAddInside");
    }*/

    /**
     * Тестирую add с граничными значениеями индексов
     */
  /*  private static void testAddIndexBounds() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, 0);
        myLinkedList.add(2, 1);
        System.out.println("Done testAddIndexBounds");

    }

    private static void testAddIncorrectIndex() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.add(0, -1);
        myLinkedList.add(0, 1);
        System.out.println("Done testAddIncorrectIndex");
    }

    private static void testAddToTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        System.out.println("Done testAddToTail");
    }*/

    /*private static void testDeleteFromHead() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.deleteFromHead();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.deleteFromHead();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        System.out.println(myLinkedList);
        myLinkedList.deleteFromHead();
        myLinkedList.deleteFromHead();
        System.out.println(myLinkedList);
        System.out.println("Done testDeleteFromHead");
    }*/
   /* private static void testDeleteFromTail() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.deleteFromTail();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.deleteFromTail();
        System.out.println(myLinkedList);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        System.out.println(myLinkedList);
        myLinkedList.deleteFromTail();
        myLinkedList.deleteFromTail();
        System.out.println(myLinkedList);
        System.out.println("Done testDeleteFromTail");
    }*/
    /*private static void testDelete() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.delete(1);
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        System.out.println(myLinkedList);
        myLinkedList.delete(1);
        System.out.println(myLinkedList);
        myLinkedList.addToTail(1);
        myLinkedList.addToTail(2);
        myLinkedList.addToTail(3);
        myLinkedList.addToTail(4);
        myLinkedList.addToTail(5);
        myLinkedList.addToTail(6);
        myLinkedList.addToTail(7);
        System.out.println(myLinkedList);
        myLinkedList.delete(1);
        myLinkedList.delete(3);
        System.out.println(myLinkedList);
        System.out.println("Done testDelete");
    }*/
}
