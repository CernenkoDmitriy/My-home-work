package HomeWork.grantForStudents.servises.impl;

import HomeWork.grantForStudents.Student;
import HomeWork.grantForStudents.servises.StudentServes;

import java.io.*;

public class ImplStudentServes implements StudentServes{

/*    private String lastName;
    private String name;
    private String middleName;
    private String numGroup;
    private int[] marks = new int[5];*/

    private String _fileName;

    public ImplStudentServes(String _fileName) {
        this._fileName = _fileName;
    }

    @Override
    public void save(Student student) {
        try (Writer writer = new FileWriter(_fileName)) {
            writer.write(student.getLastName() + System.lineSeparator());
            writer.write(student.getName()+ System.lineSeparator());
            writer.write(student.getMiddleName()+ System.lineSeparator());
            writer.write(student.getNumGroup()+ System.lineSeparator());
            for (int i = 0; i < student.getMarks().length-1; i++) {
                writer.write(student.getMarks()[i]+ System.lineSeparator());
            }
            String str = ""+student.getMarks()[4];
            writer.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Student read() {
        Student student = new Student();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(_fileName))){
            student.setLastName(bufferedReader.readLine());
            student.setName(bufferedReader.readLine());
            student.setMiddleName(bufferedReader.readLine());
            student.setNumGroup(bufferedReader.readLine());

            //То как пытался сделать я

            /*int temp = 0;
            while (bufferedReader.readLine() != null){
            int tm = bufferedReader.read();
                student.setMarks(temp, tm);
                temp++;
            }*/
            //Подсмотрел в интернете
            int [] array;
            array = bufferedReader.lines().mapToInt(Integer::parseInt).toArray();
            for (int i = 0; i < array.length; i++) {
                student.setMarks(i, array[i]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return student;
    }


}
