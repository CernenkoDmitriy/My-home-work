package HomeWork.grantForStudents;

import HomeWork.grantForStudents.servises.StudentServes;
import HomeWork.grantForStudents.servises.impl.ImplStudentServes;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Student vasay = new Student("Пупкин", "Вася", "Васильевич",
                "пм-2007", new int[]{5, 5, 5, 4, 3});
/*        Student petay = new Student("Дупкин", "Петя", "Васильевич",
                "пм-2007", new int[]{5, 4, 5, 4, 5});
        Student masha = new Student("Иванова", "Маша", "Васильевна",
                "пм-2007", new int[]{4, 4, 4, 4, 5});
        Student pasha = new Student("Куций", "Паша", "Васильевич",
                "пм-2007", new int[]{4, 4, 4, 4, 5});
        Student dima = new Student("Петров", "Дмитрий", "Васильевич",
                "пм-2007", new int[]{5, 5, 5, 4, 5});
        Student sasha = new Student("Корнев", "Саша", "Васильевич",
                "пм-2007", new int[]{5, 5, 5, 5, 5});
        Student lena = new Student("Ельцена", "Елена", "Васильевна",
                "пм-2007", new int[]{4, 4, 5, 5, 5});
        Student  dusay = new Student("Жижа", "Дуся", "Васильевна",
                "пм-2007", new int[]{3, 3, 5, 5, 5});
        Student  olay = new Student("Миря", "Ольга", "Васильевна",
                "пм-2007", new int[]{5, 0, 5, 5, 5});

        Student jora = new Student("Гупкина", "Жора", "Васильевич",
                "пм-2006", new int[]{5, 5,  4, 5});
        Student julia = new Student("Дупкин", "Юлия", "Васильевна",
                "пм-2006", new int[]{5, 5,  5, 5});
        Student kuzay = new Student("Рупкин", "Кузя", "Васильевич",
                "пм-2006", new int[]{5, 5,  4, 3});

        Student[] groupMP2007 = {vasay, petay, masha, pasha, dima, sasha, lena, dusay, olay,jora,julia,kuzay};

        Student.sortStudents(groupMP2007);
        Student.printStudents(groupMP2007);*/

        StudentServes studentServes = new ImplStudentServes("Student.std");
        studentServes.save(vasay);
        Student petay = studentServes.read();
        System.out.println(petay);
    }
}
