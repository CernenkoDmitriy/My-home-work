package HomeWork.AbstractAnimal;

import HomeWork.AbstractAnimal.Animals.Animal;
import HomeWork.AbstractAnimal.Animals.Cat;
import HomeWork.AbstractAnimal.Animals.Dog;
import HomeWork.AbstractAnimal.Animals.Frog;

public class Main {
    public static void main(String[] args) {
        Dog bax = new Dog("Бакс");
        Cat arbuz = new Cat("Арбуз");
        Frog zuzu = new Frog("Зузу");

        Animal[] animals = {bax, arbuz, zuzu};
        for (int i = 0; i < animals.length; i++) {
            animals[i].doSound();
        }
    }
}
