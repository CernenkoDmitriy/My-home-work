package HomeWork.AbstractAnimal.Animals;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void doSound() {

        System.out.println("Собака " + getName() + " говорит - Гав-гав");

    }
}
