package HomeWork.AbstractAnimal.Animals;

public class Cat extends Animal{
    public Cat (String name){
        super(name);
    }

    @Override
    public void doSound() {
        System.out.println("Кот " + getName() + " говорит - Мяу");
    }

}
