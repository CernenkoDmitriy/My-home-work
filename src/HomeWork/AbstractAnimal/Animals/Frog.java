package HomeWork.AbstractAnimal.Animals;

public class Frog extends Animal{
    public Frog(String name) {
        super(name);
    }

    @Override
    public void doSound() {
        System.out.println("Лягушка " + getName() + " говорит - Ква-ква");
    }
}
