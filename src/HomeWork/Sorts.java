package HomeWork;


import java.util.Arrays;

public class Sorts {
    public static void main(String[] args) {

        int[] babls = {8, 6, 4, 2};
        int[] select = {8, 6, 4, 2};
        int[] insert = {8, 6, 4, 2};
        int temp = 0;

        System.out.println("Сортировка выбором:");
        System.out.println("Исходный массив:" + Arrays.toString(select));
        for (int i = 0; i < select.length - 1; i++) {
            int max = 0;
            for (int j = 1; j < select.length - i; j++) {
                if (select[j] > select[max]) {
                    max = j;
                }
            }
            if (max != select.length - i - 1) {
                temp = select[select.length - i - 1];
                select[select.length - i - 1] = select[max];
                select[max] = temp;
            }
            System.out.println(i + 1 + "-й проход:" + Arrays.toString(select));
        }

        System.out.println("Пузырьковая сортировка:");
        System.out.println("Исходный массив:" + Arrays.toString(babls));
        for (int i = 0; i < babls.length - 1; i++) {
            for (int j = 0; j < babls.length - i - 1; j++) {
                if (babls[j] > babls[j + 1]) {
                    temp = babls[j + 1];
                    babls[j + 1] = babls[j];
                    babls[j] = temp;
                }
            }
            System.out.println(i + 1 + "-й проход:" + Arrays.toString(babls));
        }

        System.out.println("Сортировка вставками:");
        System.out.println("Исходный массив:" + Arrays.toString(insert));
        for (int i = 1; i < insert.length; i++) {
            temp = insert[i];
            int j;
            for (j = i - 1; j >= 0 && insert[j] > temp; j--) {
                insert[j + 1] = insert[j];
            }
            insert[j + 1] = temp;
            System.out.println(i + "-й проход:" + Arrays.toString(insert));
        }
    }
}
