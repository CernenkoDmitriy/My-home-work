package HomeWork.Passport;

import HomeWork.Passport.Data.ForeignPassport;
import HomeWork.Passport.Data.Passport;
import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException {
        Passport myPassport = new Passport ("Дмитрий", "Черненко", "AH", 123);
        System.out.println(myPassport);
        ForeignPassport myForeignPassport = new ForeignPassport(myPassport,"Dmitriy", "Chernenko");
        System.out.println(myForeignPassport);
        myForeignPassport.addVisa("To Poland", "12.12.2016", "10.05.2020");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
 /*       myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");
        myForeignPassport.addVisa("To USA", "17.11.2015", "15.04.2025");*/
       // System.out.println(myForeignPassport.printMyVises());

        //test
        Passport testPassport = (Passport) myPassport.clone();
  /*      System.out.println(testPassport);
        testPassport.setLastName("Petrov");
        testPassport.setName("Vasay");
        testPassport.setSerial("GG");
        testPassport.setNumber(321);
        System.out.println(testPassport);
        System.out.println(myPassport);*/
        ForeignPassport testFP = (ForeignPassport) myForeignPassport.clone();
        testFP.setLastName("Petrov");
        testFP.setEngLastName("Petrov");
        System.out.println(testFP);
        System.out.println(myForeignPassport);
    }
}
