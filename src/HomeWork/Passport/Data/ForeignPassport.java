package HomeWork.Passport.Data;

import java.text.ParseException;

public class ForeignPassport extends Passport implements Cloneable {
    private final int CHANGE_MY_VISA_LENGTH = 5;
    private String engName;
    private String engLastName;
    private Visa[] myVises = new Visa[CHANGE_MY_VISA_LENGTH];

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public void setEngLastName(String engLastName) {
        this.engLastName = engLastName;
    }

    private int visaCounter = 0;

    public ForeignPassport(String name, String lastName, String serial, int number, String engName, String engLastName) {
        super(name, lastName, serial, number);
        this.engName = engName;
        this.engLastName = engLastName;
    }

    public ForeignPassport(Passport passport, String engName, String engLastName) {
        super(passport.getName(), passport.getLastName(), passport.getSerial(), passport.getNumber());
        this.engName = engName;
        this.engLastName = engLastName;
    }

    public Visa[] getMyVises() {
        return myVises = myVises.clone();
    }

    public void addVisa(String typeOfVisa, String openDate, String closeDate) throws ParseException {
        if (visaCounter == myVises.length) {
            increaseArrayMyVises();
        }
        myVises[visaCounter] = new Visa(typeOfVisa, openDate, closeDate);
        visaCounter++;
    }

    private void increaseArrayMyVises() {
        Visa[] temp = new Visa[myVises.length + CHANGE_MY_VISA_LENGTH];
        for (int i = 0; i < myVises.length; i++) {
            temp[i] = myVises[i];
        }
        myVises = temp;
    }

    public StringBuilder printMyVises() {
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < visaCounter; i++) {
            temp.append("Тип визы: " + myVises[i].getTypeOfVisa() +
                    ", Дата начала: " + myVises[i].getOpenDate() +
                    ", Дата начала: " + myVises[i].getCloseDate() + "\n");
        }
        return temp;
    }

    @Override
    public String toString() {
        return String.format("Passport: name='%s', lastName='%s', serial='%s', number=%d. " +
                             "ForeignPassport: engName='%s', engLastName='%s'}\n%s"
                , getLastName(), getLastName(), getSerial(),
                getNumber(), engName, engLastName, printMyVises());
    }

    public Object clone() {
        ForeignPassport i = (ForeignPassport) super.clone();
        i.myVises = this.myVises.clone();
        return i;
    }
}
