package HomeWork.Passport.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class Visa implements Cloneable{
    private String typeOfVisa;
    private Date closeDate;
    private Date openDate;

    public Visa(String typeOfVisa, String openDate, String closeDate) throws ParseException {
        this.typeOfVisa = typeOfVisa;
        this.openDate = new SimpleDateFormat("dd.MM.yyyy").parse(openDate);
        this.closeDate = new SimpleDateFormat("dd.MM.yyyy").parse(closeDate);
    }

    public String getTypeOfVisa() {
        return typeOfVisa;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public Object clone (){
        try {
            Visa visa = (Visa) super.clone();
            visa.closeDate = (Date)super.clone();
            visa.openDate = (Date) super.clone();
            return visa;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
