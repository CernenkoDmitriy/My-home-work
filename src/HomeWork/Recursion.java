package HomeWork;

import java.util.Scanner;

public class Recursion {

    public static void main(String[] args) {
        testing ();
        int numberFromUser = gettingNumberFromUser();
        System.out.println(gettingMirrorNumber(numberFromUser));
    }

    private static int gettingNumberFromUser() {
        int result;
        System.out.println("Enter any number: ");
        Scanner s = new Scanner(System.in);
        result = s.nextInt();
        return result;
    }

    private static int gettingMirrorNumber(int numberFromUser) {
        if ((numberFromUser / 10) == 0) {
            return numberFromUser;
        }
        int factor = gettingMultipliedNumber(numberFromUser);
        int tempResult = (numberFromUser % 10) * factor;
        int rec = gettingMirrorNumber(numberFromUser / 10);
        int result = tempResult + rec;
        return result;
    }

    private static int gettingLengthNumber(int number) {
        int lengthNumber = 0;
        while (number != 0) {
            lengthNumber++;
            number /= 10;
        }
        return lengthNumber;
    }

    private static int gettingMultipliedNumber(int number) {
        int mult = 1;
        int temp = gettingLengthNumber(number);
        for (int i = 1; i < temp; i++) {
            mult *= 10;
        }
        return mult;
    }
    static void testing (){
        int [] data = {123, 123456, 987456, 809040, 99999};
        int [] expectedResult = {321, 654321, 654789, 40908, 99999};
        boolean passed = true;
        for (int i = 0; i < data.length; i++) {
            int  temp = gettingMirrorNumber(data[i]);
            if(temp!=expectedResult[i]) {
                System.out.printf("Test error: actual = %s but expected was %s ", temp, expectedResult[i]);
                passed = false;
            }
            }
        if (passed) {
            System.out.println("Tests passed");
        }
        }
    }

