package HomeWork.Ruber;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Rubber implements Iterable<String> {
    static private final int CHANGE_LENGTH = 5;
    private String[] strings;

    Rubber() {
        this.strings = new String[CHANGE_LENGTH];
    }

    public String[] getStrings() {
        return strings.clone();
    }

    public void setStrings(String[] strings) {
        if (strings == null) {
            this.strings = new String[CHANGE_LENGTH];
        }
        this.strings = strings.clone();
    }

    public void addString(String addedStrings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                strings[i] = addedStrings;
                return;
            }
        }
        String[] temp = new String[strings.length + CHANGE_LENGTH];
        for (int i = 0; i < strings.length; i++) {
            temp[i] = strings[i];
        }
        temp[strings.length] = addedStrings;
        strings = temp;

    }

    public void deleteStrings(int index) {
        try {
            if (!isIndexEmpty(index)) {
                throw new NullPointerException("Value is null");
            }
            if (!isIndexCorrect(index)) {
                throw new IndexOutOfBoundsException("output for the array border");
            }
            for (int i = index; i < strings.length - 1; i++) {
                strings[index] = strings[index + 1];
            }
            strings[strings.length - 1] = null;
            arrayReduction();
        } catch (NullPointerException | IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
    }

    private void arrayReduction() {
        int counter = 0;
        for (int i = strings.length - 5; i < strings.length; i++) {
            if (strings[i] == null) {
                counter++;
            }
        }
        if (counter == 5) {
            String[] temp = new String[strings.length - CHANGE_LENGTH];
            for (int i = 0; i < temp.length; i++) {
                temp[i] = strings[i];
            }
            strings = temp;
        }
    }

    public void changeStringValue(int index, String newStrings) {
        try {
             if (!isIndexCorrect(index)) {
                throw new IndexOutOfBoundsException("output for the array border");
            }
            strings[index] = newStrings;
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
    }

    public void moveStrings(int whatNeed, int whereNeed) {
        try {
            if (!isIndexEmpty(whatNeed)) {
                throw new NullPointerException("Value is null");
            }
            if (whereNeed > strings.length) {
                String[] temp = new String[whereNeed + 1];
                for (int i = 0; i < strings.length; i++) {
                    temp[i] = strings[i];
                }
                strings = temp;
            }
            String temp;
            temp = strings[whereNeed];
            strings[whereNeed] = strings[whatNeed];
            strings[whatNeed] = temp;
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    private boolean isIndexCorrect(int index) {
        return !(index < 0 || index >= strings.length);
    }

    private boolean isIndexEmpty(int index) {
        if (strings[index] == null) {
            return false;
        }
        return true;
    }

    @Override

    public String toString() {
        return "Rubber{" +
                "name=" + Arrays.toString(strings) +
                '}';
    }


    ////////////////////////////////////////iterator
    @Override
    public Iterator<String> iterator() {
        return new MyIterator(this);
    }

    private static class MyIterator implements Iterator<String> {

        private Rubber rubber;
        private int cursor = -1;

        MyIterator(Rubber rubber) {
            this.rubber = rubber;
        }

        @Override
        public boolean hasNext() {
            if (rubber.strings.length == 0) {
                return false;
            }
            if (cursor < rubber.strings.length - 1 || cursor > -2) {
                return true;
            }
            return false;
        }

        @Override
        public String next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            } else {
                cursor++;
            }

            return rubber.strings[cursor];
        }
    }
}
