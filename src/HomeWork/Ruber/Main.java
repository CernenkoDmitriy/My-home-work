package HomeWork.Ruber;

public class Main {
    public static void main(String[] args) {
        Rubber test = new Rubber();
        System.out.println(test);
        test.addString("3");
        System.out.println(test);
        test.addString("4");
        test.addString("5");
        System.out.println(test);
        test.deleteStrings(2);
        System.out.println(test);
        test.changeStringValue(2, "4");
        System.out.println(test);
        test.moveStrings(2,4);
        System.out.println(test);
        test.addString("3");
        System.out.println(test);
        test.moveStrings(2,9);
        System.out.println(test);
        test.moveStrings(3,9);
        System.out.println(test);
        test.deleteStrings(9);
        System.out.println(test);
        test.changeStringValue(50, "1");
        System.out.println(test);
        /////////////////////////////////////////iterator
        for (String i:test) {
            System.out.println(i);
        }
    }
}
