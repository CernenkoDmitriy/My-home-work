package HomeWork;

import java.util.Arrays;


public class ReversSorts {
    public static void main(String[] args) {
        int [] ArraysBabls = createNewArray ();
        System.out.println("Сортировка пузырьком:" + "\n" + "Исходный массив: " + printStartedArray(ArraysBabls));
        System.out.println(printArrayBabls(ArraysBabls));

        int [] ArraysSelect = createNewArray ();
        System.out.println("Сортировка выбором:" + "\n" + "Исходный массив: " + printStartedArray(ArraysSelect));
        System.out.println(printArraySelect(ArraysSelect));

        int [] ArraysInsert = createNewArray ();
        System.out.println("Сортировка вставкою:" + "\n" + "Исходный массив: " + printStartedArray(ArraysInsert));
        System.out.println(printArrayInsert(ArraysInsert));

    }
    private static int [] createNewArray(){
        return new int[]{8, 6, 4, 2};
    }
    private static String printStartedArray ( int arr []){
        String newString = Arrays.toString(arr);
        return newString;
    }
    private static String printArrayBabls ( int arr []){

        String newString = "";
        int temp = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
            newString = newString + (i + 1) + "-й проход:" + Arrays.toString(arr) + "\n";
        }
        return newString;
    }

    private static String printArraySelect ( int arr []){

        String newString = "";
        int temp = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            int max = 0;
            for (int j = 1; j < arr.length - i; j++) {
                if (arr[j] > arr[max]) {
                    max = j;
                }
            }
            if (max != arr.length - i - 1) {
                temp = arr[arr.length - i - 1];
                arr[arr.length - i - 1] = arr[max];
                arr[max] = temp;
            }
            newString = newString + (i + 1) + "-й проход:" + Arrays.toString(arr) + "\n";

        }
        return newString;
    }

    private static String printArrayInsert ( int arr []){

        String newString = "";
        int temp = 0;
        for (int i = 1; i < arr.length; i++) {
            temp = arr[i];
            int j;
            for (j = i - 1; j >= 0 && arr[j] > temp; j--) {
                arr[j + 1] = arr[j];
            }
            arr[j + 1] = temp;
            newString = newString + (i + 1) + "-й проход:" + Arrays.toString(arr) + "\n";

        }
        return newString;
    }
}
