package HomeWork.OOP;

import java.util.Scanner;

public class Rectangle {
    private int x1;
    private int y1;
    private int x2;
    private int y2;

    Rectangle(int x1, int y1, int x2, int y2) {
        setX1(x1);
        setY1(y1);
        setX2(x2);
        setY2(y2);
    }

    ;

    private void setX1(int x1) {
        this.x1 = x1;
    }

    private void setY1(int y1) {
        this.y1 = y1;
    }

    private void setX2(int x2) {
        this.x2 = x2;
    }

    private void setY2(int y2) {
        this.y2 = y2;
    }

    public int getY2() {
        return y2;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getX1() {
        return x1;
    }

    public boolean crossing(Rectangle rectangle) {

        return (x2 > rectangle.x1 && rectangle.x1 > x1) && (y1 > rectangle.y1 && rectangle.y1 > y2) ||
                (x2 > rectangle.y1 && rectangle.y1 > x1) && (y1 > rectangle.y2 && rectangle.y2 > y2);

    }

    public boolean crossPoint(Point point) {
        return (x2 >= point.getX() && point.getX() >= x1) && (y1 >= point.getY() && point.getY() >= y2);
    }

    public void scale(int temp) {
        setX2(x2 * temp);
        setY2(y2 * temp);
    }

    public void move(int temp) {
        setX2(x2 + temp);
        setY2(y2 + temp);
        setX2(x1 + temp);
        setY2(y1 + temp);
    }


}
