package HomeWork;

import java.util.Scanner;

public class PeopleWeight {
    public static void main(String[] args) {
        testingOtherPeople();
        testingFedPeople ();
        int[] amountPeople = gettingDataFromUser();
        if(testingPeopleWeight(amountPeople)){
            System.out.println("Average weight of fed people "
                    + gettingAverageAmountFedPeople(amountPeople));
            System.out.println("Average weight of other people "
                    + gettingAverageAmountOtherPeople(amountPeople));
        }
    }

    private static int gettingHowManyPpl() {
        System.out.println("Enter how many people in you group: ");
        int result;
        Scanner s = new Scanner(System.in);
        result = s.nextInt();
        return result;
    }

    private static int[] gettingDataFromUser() {
        int amount = gettingHowManyPpl();
        int[] array = new int[amount];
        System.out.println("Enter weight of person: ");
        for (int i = 0; i < array.length; i++) {
            array[i] = gettingHowManyPpl();
        }
        return array;
    }

    private static boolean testingPeopleWeight(int[] amountPeople) {
        boolean result = false;
        for (int i = 0; i < amountPeople.length; i++) {
            if (amountPeople[i] > 100) {
                return true;
            }
        }
        System.err.println("Вес хотябы одного человека должен превышать 100 кг");
        return result;
    }

    private static int gettingAverageAmountFedPeople(int[] amountPeople) {
        int averageAmount = 0;
        int counter = 0;
        for (int i = 0; i < amountPeople.length; i++) {
            if (amountPeople[i] >= 100) {
                counter++;
                averageAmount += amountPeople[i];
            }
        }
        return averageAmount / counter;
    }
    private static int gettingAverageAmountOtherPeople(int[] amountPeople) {
        int averageAmount = 0;
        int counter = 0;
        for (int i = 0; i < amountPeople.length; i++) {
            if (amountPeople[i] < 100) {
                counter++;
                averageAmount += amountPeople[i];
            }
        }
        if(averageAmount == 0){
            return  0;
        }
        return averageAmount / counter;
    }
    static void testingOtherPeople (){
        int [][] data = {
                {120},
                {40,30,60,70,100},
                {150,130,80},
                {120,140,20},
                {80,60}};
        int [] expectedResult = {0,50, 80, 20, 70};
        boolean passed = true;
        for (int i = 0; i < data.length; i++) {
            int  temp = gettingAverageAmountOtherPeople(data[i]);
            if(temp!=expectedResult[i]) {
                System.out.printf("Test error: actual = %s but expected was %s ", temp, expectedResult[i]);
                passed = false;
            }
        }
        if (passed) {
            System.out.println("Tests passed");
        }
   }
    static void testingFedPeople (){
        int [][] data = {
                {130},
                {40,30,60,70,150,158},
                {150,130,80},
                {120,140,20},
                };
        int [] expectedResult = {130,154, 140, 130};
        boolean passed = true;
        for (int i = 0; i < data.length; i++) {
            int  temp = gettingAverageAmountFedPeople(data[i]);
            if(temp!=expectedResult[i]) {
                System.out.printf("Test error: actual = %s but expected was %s ", temp, expectedResult[i]);
                passed = false;
            }
        }
        if (passed) {
            System.out.println("Tests passed");
        }
    }
}

