package HomeWork.Workers;

import HomeWork.Workers.Data.HourWorker;
import HomeWork.Workers.Data.MonthWorker;
import HomeWork.Workers.Data.Worker;

public class Main {
    public static void main(String[] args) {
        Worker[] workers = {
                new MonthWorker("Vasya", 3000),
                new HourWorker("Petys", 30),
                new MonthWorker("Grisha", 2500),
                new HourWorker("Chuck", 3000),
                new MonthWorker("Anatoliy", 1500),
                new MonthWorker("Valentin", 7000),
                new HourWorker("Alena", 60)
        };

        int n = workers.length;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                if(workers[j-1].countSalary()
                        .compareTo(workers[j].countSalary()) < 0){
                    Worker temp = workers[j - 1];
                    workers[j-1] = workers[j];
                    workers[j] = temp;
                }
            }
        }

        for (int i = 0 ; i < 5 && i < workers.length ; ++i) {
            System.out.println(workers[i].getName());
        }

        for (int i = workers.length - 3 < 0 ? 0 : workers.length - 3
             ; i < workers.length
                ; ++i) {

            System.out.println(workers[i].getId());

        }

    }
}
