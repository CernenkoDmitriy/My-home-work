package HomeWork.Workers.Data;


import java.math.BigDecimal;
import java.math.MathContext;

public class HourWorker extends Worker {
    public HourWorker() {
    }

    public HourWorker(String name, int rate) {
        super(name, rate);
    }

    @Override
    public BigDecimal countSalary() {
        BigDecimal rate = getRate();
        BigDecimal hoursInDay = new BigDecimal(8, MathContext.DECIMAL64);
        BigDecimal workingDaysInMonth = new BigDecimal(20.8, MathContext.DECIMAL64);
        return hoursInDay.multiply(workingDaysInMonth)
                .multiply(rate);
    }
}
