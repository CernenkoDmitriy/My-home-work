package HomeWork.Workers.Data;

import java.math.BigDecimal;

public class MonthWorker extends Worker  {
    public MonthWorker() {
    }

    public MonthWorker(String name, int rate) {
        super(name, rate);
    }

    @Override
    public BigDecimal countSalary() {
        return getRate();
    }
}
