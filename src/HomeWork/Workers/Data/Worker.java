package HomeWork.Workers.Data;
import java.math.BigDecimal;
import java.math.MathContext;

    public abstract class Worker  {
        private static long counter;

        private long _id;
        private String _name;
        private BigDecimal _rate;


        public Worker() {
            this("Unknown", 0);
        }

        public Worker(String name, int rate) {
            _id = ++counter;
            _name = name;
            _rate = new BigDecimal(rate, MathContext.DECIMAL64);
        }

        public long getId() {
            return _id;
        }

        public String getName() {
            return _name;
        }

        public void setName(String name) {
            _name = name;
        }

        public BigDecimal getRate() {
            return _rate;
        }

        public void setRate(BigDecimal rate) {
            _rate = rate;
        }

        public abstract BigDecimal countSalary();

          @Override
        public String toString() {
            return "id=" + _id +
                    ", name='" + _name +
                    ", rate=" + _rate +
                    ", salary = " + this.countSalary();
        }

}
