package HomeWork.Fractions;

public class Fractions {
    private long entirePart;
    private short fractionPart;
    private int sign;
    private int amountFractionNumbers;


    public long getEntirePart() {
        return entirePart;
    }

    public void setEntirePart(long entirePart) {
        if (entirePart<0){
            entirePart *=(-1);
        }
        this.entirePart = entirePart;
    }

    public short getFractionPart() {
        return fractionPart;
    }

    public void setFractionPart(short fractionPart) {
        if (fractionPart<0){
            fractionPart *=(-1);
        }
        this.fractionPart = fractionPart;
    }


    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        if (sign == 1 || sign == (-1)) {
            this.sign = sign;
        }
    }

    public int getAmountFractionNumbers() {
        return amountFractionNumbers;
    }

    public void setAmountFractionNumbers(int amountFractionNumbers) {
        int temp = 0;
        int fractionPart = getFractionPart();
        while (fractionPart > 0) {
            temp += 1;
            fractionPart /= 10;
        }
        if (amountFractionNumbers >= temp) {
            this.amountFractionNumbers = amountFractionNumbers;
        }
    }

    Fractions(long entirePart, short fractionPart, int amountFractionNumbers, int sign) {
        setEntirePart(entirePart);
        setFractionPart(fractionPart);
        setAmountFractionNumbers(amountFractionNumbers);
        setSign(sign);
    }

    public long transformationIntoLong() {
        long transformAmountFractionNumbers = 10;
        for (int i = 1; i < amountFractionNumbers; i++) {
            transformAmountFractionNumbers *= 10;
        }
        long transformFractionPart = fractionPart;
        long result = (entirePart * transformAmountFractionNumbers + transformFractionPart);
        return result;
    }

    private void transformationIntoObject(Fractions fractions, long result) {
        long tempLong = result;
        for (int i = 0; i < getAmountFractionNumbers(); i++) {
            tempLong /= 10;
        }
        setEntirePart(tempLong);
        long temp;
        long tempResult = 0;
        long counter = 1;
        for (int i = 1; i <= getAmountFractionNumbers(); i++) {
            temp = result % 10;
            temp *= counter;
            tempResult = tempResult + temp;
            counter *= 10;
            result /= 10;
        }
        while (tempResult > 32767) {
            tempResult /= 10;
            setAmountFractionNumbers(getAmountFractionNumbers() - 1);
        }
        long checkTempResult;
        for (int i = 0; i < getAmountFractionNumbers(); i++) {
            checkTempResult = tempResult%10;
            if(checkTempResult==0){
                tempResult /=10;
                setAmountFractionNumbers(getAmountFractionNumbers()-1);
            }
        }
        setFractionPart((short) tempResult);
    }

    public void multiplication(Fractions fractions) {
        long result = transformationIntoLong() * fractions.transformationIntoLong();
        setSign(getSign() * fractions.getSign());
        setAmountFractionNumbers(getAmountFractionNumbers() + fractions.getAmountFractionNumbers());
        transformationIntoObject(fractions, result);
    }

    public void addition(Fractions fractions) {
        long tempCurrent = transformationIntoLong();
        long tempAdded = fractions.transformationIntoLong();

        int difAmountFractionNumbers = getAmountFractionNumbers() - fractions.getAmountFractionNumbers();
        int tempMult = 10;
        if (difAmountFractionNumbers > 0) {
            for (int i = 0; i < difAmountFractionNumbers; i++) {
                tempAdded *= tempMult;
            }
        }
        if (difAmountFractionNumbers < 0) {
            difAmountFractionNumbers *= (-1);
            for (int i = 0; i < difAmountFractionNumbers; i++) {
                tempCurrent *= tempMult;
            }
        }
        long result = (tempCurrent * getSign()) + (tempAdded * fractions.getSign());

        marking(fractions, result);

        transformationIntoObject(fractions, result);
    }

    public void subtraction(Fractions fractions) {
        long tempCurrent = transformationIntoLong();
        long tempAdded = fractions.transformationIntoLong();

        int difAmountFractionNumbers = getAmountFractionNumbers() - fractions.getAmountFractionNumbers();
        int tempMult = 10;
        if (difAmountFractionNumbers > 0) {
            for (int i = 0; i < difAmountFractionNumbers; i++) {
                tempAdded *= tempMult;
            }
        }
        if (difAmountFractionNumbers < 0) {
            difAmountFractionNumbers *= (-1);
            for (int i = 0; i < difAmountFractionNumbers; i++) {
                tempCurrent *= tempMult;
            }
        }
        long result = (tempCurrent * getSign()) - (tempAdded * fractions.getSign());

        marking(fractions, result);

        transformationIntoObject(fractions, result);
    }

    public boolean compare(Fractions fractions) {
        return entirePart == fractions.getEntirePart() && fractionPart == fractions.getFractionPart() &&
                sign == fractions.getSign() && amountFractionNumbers == fractions.getAmountFractionNumbers();
    }

    private void marking(Fractions fractions, long result) {
        if (result < 0) {
            result *= (-1);
            setSign(-1);
        } else {
            setSign(1);
        }

        if (getAmountFractionNumbers() > fractions.getAmountFractionNumbers()) {
            setAmountFractionNumbers(getAmountFractionNumbers());
        }
        if (getAmountFractionNumbers() < fractions.getAmountFractionNumbers()) {
            setAmountFractionNumbers(fractions.getAmountFractionNumbers());
        }
    }

    public void print() {
        String zeros = "";
        String signOfNumber;
        int temp = 0;
        int tempFractionPart = getFractionPart();
        while (tempFractionPart > 0) {
            temp += 1;
            tempFractionPart /= 10;
        }
        while (getAmountFractionNumbers() > temp) {
            zeros += 0;
            temp++;
        }
        if (sign > 0) {
            signOfNumber = "";
        } else {
            signOfNumber = "-";
        }
        System.out.println("Ваше число: " + signOfNumber + entirePart + "," + zeros + fractionPart);
    }

    public static void test() {
        Fractions x = new Fractions(3, (short) 5, 2, 1);
        Fractions y = new Fractions(9, (short) 8, 1, 1);
        Fractions resultMultiplication = new Fractions(29, (short) 89, 2, 1);
        boolean passed = true;
        x.multiplication(y);
        if (!x.compare(resultMultiplication)) {
            System.err.printf("Test error");
        }
        x.addition(y);
        Fractions addition = new Fractions(39, (short) 69, 2, 1);
        if (!x.compare(addition)) {
            System.err.printf("Test error");
        }
        Fractions z = new Fractions(5, (short) 5, 3, -1);
        Fractions subtraction = new Fractions(44, (short) 695, 3, 1);
        x.subtraction(z);
        if (!x.compare(subtraction)) {
            System.err.printf("Test error");
        }
        System.out.println("Test passed");
    }

}
