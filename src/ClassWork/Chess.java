package ClassWork;

import java.util.Scanner;

public class Chess {
    public static void main(String[] args) {
        System.out.println("Ввидете номер фигуры: 1. Ладья, 2. Офицер, 3. Королева, 4. Король, 5. Белая пешка, " +
                "6. Черная пешка, 7. Конь");
        int figure = getFigureDataFromUser();
        System.out.println("Введите координаты Вашей фигуры: горизонталь, вертикаль");
        int[] positionYour = getLocationDataFromUser();
        System.out.println("Введите координаты вражеской фигуры: горизонталь, вертикаль");
        int[] positionEnemy = getLocationDataFromUser();
        if (gettingResult(figure, positionYour[0], positionYour[1], positionEnemy[0], positionEnemy[1])) {
            System.out.println("Ваша фигура угражает врежеской фигуре");
        } else {
            System.out.println("Ваша фигура не угражает врежеской фигуре");
        }

    }

    private static int[] getLocationDataFromUser() {
        Scanner s = new Scanner(System.in);
        int[] result = new int[2];
        result[0] = s.nextInt();
        result[1] = s.nextInt();
        return result;
    }

    private static int getFigureDataFromUser() {
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }

    private static boolean checkTheThreatFromTheRook(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return youPositionA == EnemyA || yourPositionB == EnemyB;
    }

    private static boolean checkTheThreatFromTheOfficer(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return (youPositionA - EnemyA) == (yourPositionB - EnemyB);
    }
    private static boolean checkTheThreatFromTheQueen(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return (youPositionA - EnemyA) == (yourPositionB - EnemyB)&&youPositionA == EnemyA || yourPositionB == EnemyB;
    }
    private static boolean checkTheThreatFromTheQKing(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return (youPositionA - EnemyA) == 0 || (youPositionA - EnemyA) == -1 || (youPositionA - EnemyA) == 1 &&
                (yourPositionB - EnemyB) == 0  || (yourPositionB - EnemyB) == -1 || (yourPositionB - EnemyB) == 1;
    }
    private static boolean checkTheThreatFromTheWhitePawn(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return (youPositionA - EnemyA)== 1 || (youPositionA - EnemyA)== -1 && (yourPositionB - EnemyB)==-1;
    }
    private static boolean checkTheThreatFromTheBlackPawn(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return (youPositionA - EnemyA)== 1 || (youPositionA - EnemyA)== -1 && (yourPositionB - EnemyB)==1;
    }
    private static boolean checkTheThreatFromTheHoers(int youPositionA, int yourPositionB, int EnemyA, int EnemyB) {
        return ((youPositionA - EnemyA)== -2 || (youPositionA - EnemyA)== 2 &&
                (yourPositionB - EnemyB)==1 || (yourPositionB - EnemyB)==-1) ||
                ((youPositionA - EnemyA)== -1 || (youPositionA - EnemyA)== 1 &&
                (yourPositionB - EnemyB)==2 || (yourPositionB - EnemyB)==-2);
    }
    private static boolean gettingResult(int figure, int youPositionA, int youPositionB, int EnemyA, int EnemyB) {
        switch (figure) {
            case 1:
                return checkTheThreatFromTheRook(youPositionA, youPositionB, EnemyA, EnemyB);
            case 2:
                return checkTheThreatFromTheOfficer(youPositionA, youPositionB, EnemyA, EnemyB);
            case 3:
                return checkTheThreatFromTheQueen(youPositionA, youPositionB, EnemyA, EnemyB);
            case 4:
                return checkTheThreatFromTheQKing(youPositionA, youPositionB, EnemyA, EnemyB);
            case 5:
                return checkTheThreatFromTheWhitePawn(youPositionA, youPositionB, EnemyA, EnemyB);
            case 6:
                return checkTheThreatFromTheBlackPawn(youPositionA, youPositionB, EnemyA, EnemyB);
            case 7:
                return checkTheThreatFromTheHoers(youPositionA, youPositionB, EnemyA, EnemyB);
            default:
                System.out.println("Вы ввели неверное название фигуры");
                return false;
        }
    }
}
