package ClassWork;

import java.util.Arrays;
import java.util.Scanner;

public class ClassWork {
    static final int YEAR = 0;
    static final int MONTH = 1;
    static final int DAY = 2;
    static final int NUM_OF_MONTH = 12;

    public static void main(String[] args) {
        testTomorrow();
        testYesterday();
        int[] dateFromUser = askDateFromUser();
        System.out.println("Today is " + dateToString(dateFromUser));

        int[] yesterday = countYesterday(dateFromUser);
        System.out.println("yesterday was " + dateToString(yesterday));

        int[] tomorrow = countTomorrowDay(dateFromUser);
        System.out.println("yesterday was " + dateToString(tomorrow));
    }

    public static int[] askDateFromUser() {
        String[] dates = {"Enter year", "Enter month", "Enter day"};
        int[] dateFromUser = new int[3];
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < dateFromUser.length; i++) {
            System.out.println(dates[i]);
            dateFromUser[i] = s.nextInt();
        }
        return dateFromUser;
    }


    public static int[] countYesterday(int[] date) {
        int[] year = month();

        int[] result = new int[date.length];
        result[YEAR] = date[YEAR];
        result[MONTH] = date[MONTH];
        result[DAY] = date[DAY];

        if (isLeapYear(result[YEAR])) {
            year[1] = year[1] + 1;
        }

        result[DAY] = result[DAY] - 1;
        if (result[DAY] < 1) {
            result[MONTH] = result[MONTH] - 1;
            if (result[MONTH] != 0) {
                result[DAY] = year[result[MONTH] - 1];
            } else if (result[MONTH] < 1) {
                result[DAY] = 31;
                result[MONTH] = 12;
                result[YEAR] = result[YEAR] - 1;
            }
        }
        return result;
    }

    static String dateToString(int[] yesterday) {
        String result = Arrays.toString(yesterday);
        return result;
    }

    private static boolean isLeapYear(int year) {
        if (year % 400 == 0) {
            return true;
        }
        if (year % 100 == 0) {
            return false;
        }
        if (year % 4 == 0) {
            return true;
        }

        return false;
    }

    public static int[] countTomorrowDay(int[] date) {
        int[] year = month();

        int[] result = new int[date.length];
        result[YEAR] = date[YEAR];
        result[MONTH] = date[MONTH];
        result[DAY] = date[DAY];

        if (isLeapYear(result[YEAR])) {
            year[1] = year[1] + 1;
        }

        result[DAY] = result[DAY] + 1;
        if (result[DAY] > year[result[MONTH] - 1]) {
            result[MONTH] = result[MONTH] + 1;
            result[DAY] = 1;
            if (result[MONTH] > 12) {
                result[DAY] = 1;
                result[MONTH] = 1;
                result[YEAR] = result[YEAR] + 1;
            }
        }
        return result;
    }

    private static int[] month() {
        int[] year = new int[NUM_OF_MONTH];
        year[0] = 31;
        year[1] = 28;
        year[2] = 31;
        year[3] = 30;
        year[4] = 31;
        year[5] = 30;
        year[6] = 31;
        year[7] = 31;
        year[8] = 30;
        year[9] = 31;
        year[10] = 30;
        year[11] = 31;
        return year;
    }
    private static void testYesterday() {
        int [][] data = {
                {3, 4, 5},
                {3, 1, 1},
                {3, 12, 31},
                {3, 3, 1},
                {4, 3, 1}
        };

        int [][] expectedResultsYesterday = {
                {3, 4, 4},
                {2, 12, 31},
                {3, 12, 30},
                {3, 2, 28},
                {4, 2, 29}
        };
        boolean passed = true;
        for (int i = 0; i < data.length; ++i) {
            int[] actual = countYesterday(data[i]);
            if (!Arrays.equals(actual, expectedResultsYesterday[i])) {
                System.err.printf("Test error: actual = %s but expected was %s ",
                        Arrays.toString(actual),
                        Arrays.toString(expectedResultsYesterday[i]));
                passed = false;
            }
        }
        if (passed) {
            System.out.println("Tests passed");
        }
    }
    private static void testTomorrow() {
        int [][] data = {
                {3, 4, 5},
                {3, 2, 28},
                {3, 12, 31},
                {4, 2, 28},
                {4, 2, 29}
        };

        int [][] expectedResultsTomorrow = {
                {3, 4, 6},
                {3, 3, 1},
                {4, 1, 1},
                {4, 2, 29},
                {4, 3, 1}
        };
        boolean passed = true;
        for (int i = 0; i < data.length; ++i) {
            int[] actual = countTomorrowDay(data[i]);
            if (!Arrays.equals(actual, expectedResultsTomorrow[i])) {
                System.err.printf("Test error: actual = %s but expected was %s ",
                        Arrays.toString(actual),
                        Arrays.toString(expectedResultsTomorrow[i]));
                passed = false;
            }
        }
        if (passed) {
            System.out.println("Tests passed");
        }
    }

}
