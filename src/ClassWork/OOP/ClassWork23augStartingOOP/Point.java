package ClassWork.OOP.ClassWork23augStartingOOP;

import java.util.Comparator;

public class Point implements Comparable<Point>{
    private static int counter = 0;

    private String name;
    private int coordinateX;
    private int coordinateY;
    private int id = 0;

    Point (String name, int x, int y){
        this.coordinateX = x;
        this.coordinateY = y;
        this.name = name;
        this.id = ++counter;
    }

    public int getId () {
        return id ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public double calculatingDistanceFromZero() {
        return Math.sqrt(((coordinateX * coordinateX) + (coordinateY * coordinateY)));
    }


    ////старая сортировка
   /* public static void sortPint(Point[] points) {
        for (int i = 0; i < points.length; i++) {
            System.out.println(points[i].name + " " + points[i].calculatingDistanceFromZero());
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = 1; j < points.length ; j++) {
                if (points[j - 1].calculatingDistanceFromZero() > points[j].calculatingDistanceFromZero()) {
                    Point temp = points[j - 1];
                    points[j - 1] = points[j];
                    points[j] = temp;
                }
            }

        }
    }*/

    public static void toPrint(Point[] points) {
        System.out.println("Имя\t\t\tКоордината Х\tКоордината У\tid");
        System.out.println("--------------------------------------------------");
        for (int i = 0; i < points.length; ++i) {
            System.out.printf("%s\t\t\t\t%d\t\t%d\t\t\t\t%d%n",
                    points[i].name, points[i].coordinateX, points[i].coordinateY, points[i].id);
        }
    }

    @Override
    public int compareTo(Point o) {
        return  (int) ((this.calculatingDistanceFromZero() - o.calculatingDistanceFromZero())*100);
    }
}

class PointByIdComparator implements Comparator <Point>{
    @Override
    public int compare(Point o1, Point o2) {
        return o1.getId()-o2.getId();
    }
}
class PointByReverseDistanceComparator implements Comparator <Point>{

    @Override
    public int compare(Point o1, Point o2) {
        return (int) ((o2.calculatingDistanceFromZero() - o1.calculatingDistanceFromZero())*100);
    }
}
