package ClassWork.OOP.ClassWork23augStartingOOP;

import java.util.Arrays;

public class CalculateDistancePoint {
    public static void main(String[] args) {
        Point pointOne = new Point("First",14,16);
        Point pointTwo = new Point("Second",13,9);
        Point pointThree = new Point("Third",6,10);
        Point pointFourth = new Point("Fourth",1,1);
        Point pointFifth = new Point("Fifth",2,2);
        Point pointSixth = new Point("Sixth",1,2);

        Point[] points = {pointOne, pointTwo, pointThree, pointFourth, pointFifth, pointSixth};
        System.out.println("Без сортировки");
        Point.toPrint(points);
        Arrays.sort(points);
        System.out.println("Сортировка по возрастанию расстояния от начала координат");
        Point.toPrint(points);
        Arrays.sort(points, new PointByReverseDistanceComparator());
        System.out.println("Сортировка по убыванию расстояния от начала координат");
        Point.toPrint(points);
        Arrays.sort(points, new PointByIdComparator());
        System.out.println("Сортировка по id");
        Point.toPrint(points);
    }






}
