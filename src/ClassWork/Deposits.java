package ClassWork;

import java.util.Scanner;

public class Deposits {
    static final int  depositAmount = 0;
    static final int  percentRate = 1;
    static final int  wishesAmountInMonth = 2;
    static final int  wishesDepositAmount = 3;

    public static void main(String[] args) {
        test();
        //Условия
        int[] dataFromUser = dataAssing();
        //Решение
        System.out.println("Вы будете получать желаемую сумму от процентов через "
                + countWishesAmountInMonth(dataFromUser[depositAmount], dataFromUser[percentRate], dataFromUser[wishesAmountInMonth]) + " месяцев");
        System.out.println("Вы накопите желаемую сумму от процентов через "
                + countWishesDepositAmount(dataFromUser[depositAmount], dataFromUser[percentRate], dataFromUser[wishesDepositAmount]) + " месяцев");
    }

    static int countWishesAmountInMonth( int depositAmount, int percentRate, int wishesAmountInMonth) {
        int monthAmount = 0;
        for (int currentIncrease = 0; currentIncrease < wishesAmountInMonth; ) {
            currentIncrease = (depositAmount * percentRate) / 100;
            depositAmount += currentIncrease;
            monthAmount++;
        }
        return monthAmount;
    }

    static int countWishesDepositAmount(int depositAmount, int percentRate, int totalAmount) {
        int monthAmount = 0;
        for (; depositAmount < totalAmount;) {
            int addedPercent = (depositAmount * percentRate) / 100;
            depositAmount += addedPercent;
            monthAmount ++;
        }
        return monthAmount;
    }

    static int askDateFromUser() {
        int data;
        Scanner i = new Scanner(System.in);
        data = i.nextInt();
        return data;
    }

    static int[] dataAssing() {
        int[] result = new int[4];
        System.out.println("Уточните сумму вкалада: ");
        result[0] = askDateFromUser();
        System.out.println("Уточните процентнцую ставку: ");
        result[1] = askDateFromUser();
        System.out.println("Уточните сумму котрую хотите получить от процентов: ");
        result[2] = askDateFromUser();
        System.out.println("Уточните желаему сумму которую хотите накопить: ");
        result[3] = askDateFromUser();
        return result;
    }

    static void test() {
        int[][] startedData = {
                {1000, 3, 40, 1400},
                {2000, 5, 50, 2100},
                {1000, 5, 120, 1200}
        };
        int[][] result = {
                {11, 12},
                {1, 1},
                {20, 4}
        };

        boolean passed = true;
        for (int i = 0; i < startedData.length; i++) {

            if (countWishesAmountInMonth( startedData[i][0], startedData[i][1], startedData[i][2]) != result[i][0] ||
                    countWishesDepositAmount(startedData[i][0], startedData[i][1], startedData[i][3]) != result[i][1]) {
                System.err.println("Error");
                passed = false;
            }
        }
        if (passed) {
            System.out.println("Tests passed");
        }
    }
}
